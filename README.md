LUCID

LUCID is a dream journaling website, meant to increase recollection of a users dreams and provide more awareness in the dream state. It allows a user to log in via their Facebook account and save dream entries, as well as delete them. Along with the basic dream journaling feature, LUCID provides some general information about lucid dreaming, and in future will feature much more content about lucid dreaming, including popular 'reality checks', as well as some handy data visualization tools.

The app should be fairly straightforward to use. Once you load the app, you will be greeted with a little info about LUCID, and you will have the option to click the Info link for more general info about lucid dreaming. However, in order to create a dream entry, you must be signed into your Facebook account. Once you are signed in, you will be redirected to the dream index page, showing you all of your previous (if any) dream entries. Click the green (+) button in order to be taken to the posting form.

At this time, I am unaware of any bugs with my app, but if you happen to find any, please let me know!


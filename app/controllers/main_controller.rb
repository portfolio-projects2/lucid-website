class MainController < ApplicationController
  before_action :current_user
  def index
    if @current_user
      redirect_to '/dreams'
    end
  end

  def show
    
  end
end

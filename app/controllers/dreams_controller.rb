class DreamsController < ApplicationController
  before_action :current_user
  def index
    if @current_user
      @dreams = Dream.all
    else
      redirect_to '/'
    end
  end

  def info
    @dreams = Dream.all
  end

  def new
    @dream = Dream.new
  end

  def create
    @current_user.dreams.create(dream_params)
    redirect_to '/dreams'
  end

  def show
    id = params[:id]
  end

  def destroy
    Dream.destroy(params[:id])
    redirect_to dreams_path
  end

  private

  def dream_params
    params.require(:dream).permit(:title, :summary, :reflection, :lucidity, :signs)
  end
end

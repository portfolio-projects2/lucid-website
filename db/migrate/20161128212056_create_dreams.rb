class CreateDreams < ActiveRecord::Migration
  def change
    create_table :dreams do |t|
      t.string :title
      t.text :summary
      t.text :reflection
      t.boolean :lucidity
      t.string :signs
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
